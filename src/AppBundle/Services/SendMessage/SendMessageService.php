<?php
namespace AppBundle\Services\SendMessage;

class SendMessageService{
    /**
    * Message's body
    * @var object of \AppBundle\Message\Message
    */     
    protected $emailMessage;
    /**
    * Twig
    * @var object of Twig templates
    */      
    protected $templatingService;
    /**
    * Swift Mailer
    * @var object of Swift Mailer
    */      
    protected $swiftMailerService;
    /**
    * Smtp provider email address
    * @var string
    */    
    protected $fromSmtpAccount;
    /**
    * Site admin's email address
    * @var string
    */
    protected $toAdmin;
    /**
    * The constructor receives parameters from files parameters.yml & services.yml , 
    * this behavior was defined in config file services.yml => google.capthca.service
    * 
    * @param $mailer private key for your site
    * @param $templatingService
    * @param $fromSmtpAccount
    * @param $toAdmin
    * @return null
    */    
    public function __construct($mailer, $templatingService, $fromSmtpAccount, $toAdmin){
        $this->swiftMailerService = $mailer;
        $this->fromSmtpAccount = $fromSmtpAccount;
        $this->toAdmin = $toAdmin;
        $this->templatingService = $templatingService;
    }
    
    public function setMessangeToAdmin(\AppBundle\Message\Message $message){
        $messageObject = $this
            ->getMessangeObject(
                "Message from the portfolio's site",
                $this->fromSmtpAccount,
                $this->toAdmin
                )
            ->setBody(
                 $this->templatingService->render('message/message.html.twig',[
                     'message' => $message
                     ]),
                     'text/html'
                );
        $this->emailMessage = $messageObject;
        return true;
    }
    
    public function getMessangeObject($subject, $from, $to){
        $emailMessage = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($from)
            ->setTo($to);
        return $emailMessage;
    }
    
    public function sendMessage(){
        $this->swiftMailerService->send(
            $this->emailMessage);
    }
}
    
