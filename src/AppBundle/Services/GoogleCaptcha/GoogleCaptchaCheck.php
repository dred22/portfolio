<?php
namespace AppBundle\Services\GoogleCaptcha;

use AppBundle\Services\GoogleCaptcha\ReCaptcha\ReCaptcha;

class GoogleCaptchaCheck{
    /**
    * Encapsulated an ReCaptcha object
    * 
    * @var object of AppBundle\Services\GoogleCaptcha\ReCaptcha
    */
    protected $reCaptcha;
    /**
    * The constructor receives parameter from file parameters.yml => google_site_key,
    * this behavior was defined in config file services.yml => google.capthca.service
    * 
    * @param $googleSecretKey private key for your site
    * @return null
    */
    public function __construct($googleSecretKey){
        $this->reCaptcha = new ReCaptcha($googleSecretKey);
    }
    /**
    * Calls the reCAPTCHA siteverify API to verify whether the user passes CAPTCHA test.
    * 
    * @param $request Object type of \Symfony\Component\HttpFoundation\Request
    * @return $response Object type of AppBundle\Services\GoogleCaptcha\ReCaptcha\Response
    */
    public function verifyResponse(\Symfony\Component\HttpFoundation\Request $request){
        $response = $this->reCaptcha->verify(
            $request->request->get('g-recaptcha-response'),
            $request->getClientIp()
            );
        return $response;
    }
}
    
