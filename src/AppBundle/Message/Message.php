<?php

namespace AppBundle\Message;

use Symfony\Component\HttpKernel\Exception\HttpException;
/**
 * Message class wich will bu use to treat message form
 */

class Message{
    /**
    * Sender's name
    * 
    * @var string 
    */
    protected $name;
    /**
     * Sender's email
     * @var string 
     */
    protected $email;
    /**
     * Sender's phone number,  constraint -> numbers and one '+' possible
     * 
     * @var string
     */
    protected $phoneNumber;
    /**
     * @var string
     */
    protected $message;
    /**
    * @return $name string
    */    
    public function getName(){
        return $this->name;
    }
    /**
    * @return $email string
    */
    public function getEmail(){
        return $this->email;
    }
    /**
    * @return $phoneNumber string
    */
    public function getPhoneNumber(){
        return $this->phoneNumber;
    }
    /**
    * @return $message string
    */
    public function getMessage(){
        return $this->message;
    }
    
    /**
    * @param $name string
    * @return self
    */
    public function setName($name){
        $this->name = $name;
        return $this;
    }
    /**
    * @param $email string with constraint -> email format
    * @return self
    */
    public function setEmail($email){
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new HttpException(500, "The type of email is not valid.");
        }
        
        $this->email = $email;
        return $this;
    }
    /**
    * @param $phoneNumber string 
    * @return self
    */
    public function setPhoneNumber($phoneNumber){
        $this->phoneNumber = $phoneNumber;
        return $this;
    }
    /**
    * @param $message string 
    * @return self
    */
    public function setMessage($message){
        $this->message = $message;
        return $this;
    }
}