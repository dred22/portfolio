<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\Contact;
use AppBundle\Message\Message;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        //get content for section About
        $entity = $em->getRepository('AppBundle:About')->find(1);
            
        // Creating an object $message wich we will use to create aur form object
        $message = new Message;
        
        //Getting forms creator
        $formFactory = $this->get('form.factory');
        
        //Creating new form from our object-model $message, to do that the from creatot use a class Contact   
        $form = $formFactory->create(Contact::class, $message);
        
        //Getting HTTP request variables for the form
        $handle = $form->handleRequest($request);
        
        //Managing the Symfony session object
        $session = $request->getSession();
        
        //Checking if the form was submited
        if ($handle->isSubmitted()){
            //if the form is valid we will sent a message and put flashBag messange in the page
            $captchaGooleCheck = $this->get('google.capthca.service')->verifyResponse($request);
            if ($handle->isValid() && $captchaGooleCheck->isSuccess()) {
                //putting flashBag messange
                $session->getFlashBag()->add('message','Votre message a été bien envoyré');
                //get mail sending service
                $sentMessangeService =  $this->get('send.message.service');
                //preparea message for site admin
                $sentMessangeService->setMessangeToAdmin($message);
                //send the message
                $sentMessangeService->sendMessage();
 
            //if the form in not valid we will put error flashBag message in the page
            } else {
                //put flashBag messange
                $session->getFlashBag()->add('error','Votre message n\'a pes été envoyré');
                //put flashBag code errors of reCaptcha
                foreach ($captchaGooleCheck->getErrorCodes() as $code){
                    $session->getFlashBag()->add('error','capthca -->'.$code);
                }
            }
        }

        return $this->render('portfolio/index.html.twig', [
            'form'    => $form->createView(),
            'content' => $entity->getContent()
        ]);
    }
}
