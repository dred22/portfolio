var positionScrollY = $(window).scrollTop();
var hauteurFenetre  = $(window).outerHeight();
var hauteurTotale   = $(document).outerHeight();
var heightOfNav = $('nav').height(); 
var verifierAnimation = function ()
{
        $("[data-animate]").each(function(){

            var positionElementY = $(this).offset().top;
            var positionElementX = $(this).offset().left;
            
            var animation = $(this).attr("data-animate");
            var delay = $(this).attr("data-delay");
            //(delay ? '' :delay = 1 )
            if (delay == undefined){
                delay = 1;
            }
            

            
            // L'ELEMENT ESY VISIBLE SI SA positionElementY
            // EST AU DESSUS DU BAS DE L'ECRAN (positionScrollY + hauteurFenetre)
            // ET EN DESSOUS DU HAUT DE L'ECRAN (positionScrollY)

            if ( (positionElementY <= (positionScrollY + hauteurFenetre)) 
                    && (positionElementY  >= (positionScrollY )) )
            {
                // VISIBLE
                // AJOUTER LA CLASSE animated POUR DECLENCHER L'ANIMATION
                setTimeout(function(e){
                    $(e).addClass("animated " + animation);
                },delay,this)
                
            }
            else
            {
                // INVISIBLE
                // ENLEVR LA CLASSE animated POUR ARRETER L'ANIMATION
                $(this).removeClass("animated " + animation);
            }
        });
    
};

$(function(){
    $(window).on("scroll", function(){
        // CETTE VALEUR CHANGE PUISQU'ON VIENT DE SCROLLER
        positionScrollY = $(window).scrollTop();
        // CES VALEURS PEUVENT AVOIR CHANGE SI LA FENETRE A ETE REDIMENSIONNE
        hauteurFenetre  = $(window).outerHeight();
        hauteurTotale   = $(document).outerHeight();
        
        // DEBUG
        /*var info = hauteurTotale + "=" + hauteurFenetre + "+" + positionScrollY;
        $(".info").html(info);*/
        
        // VERIFIER POUR TOUTES LES BALISES A ANIMER
        // SI ELLES SONT VISIBLES
        verifierAnimation();
    });
    
    // ON LANCE LA VERIFICATION AUSSI AU CHARGEMENT DE LA PAGE
    verifierAnimation();
});

$(function() {
    $('.page-scroll').hover(function(){
        $(this).toggleClass('animated jello');
    })
    $('.portfolio-item').hover(function(){
        $(this).toggleClass('animated jello');
    })
})